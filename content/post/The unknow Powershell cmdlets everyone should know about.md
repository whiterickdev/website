---
title: "The Unknown Powershell Cmdlets Everyone Should Know About"
date: 2019-11-22T15:56:43-06:00
lastmod: 2019-11-22T15:56:43-06:00
tags : ["Powershell"]
categories : [ "dev" ]
layout: post
type:  "post"
highlight: false
draft: false
---


![cover img](/Img/helloWorldPS.png)


Have you ever thought...I wish I have known this earlier!? With PowerShell, it is pretty common not to be aware of some of the features or options it has. It takes the time read the documentation and at the time you (I) find a way to do X, you (I) will probably skip reading the documentation.
However, across experience and seeing others code, I have found some cmdlets, I wish I have known before! And it seems not everybody knows about them!
Today, I want to share these little secrets that have made my Powershell time a lot more enjoyable and hopefully make your life a bit easier.

I will be given the code before I explain what it does, I invite you to execute the code before reading the rest! So I don't spoil you the surprises!

## Show-Command

```powershell
PS> Show-Command Set-Content
```

Yes...that's right! Now you have auto-generated GUI for any cmdlet you want, even the ones from custom modules! This comes pretty handy if some of the cmdlets your are not quite use to use.

![shwCommand Screenshot](/Img/showCommand.png)

When you click "run", it actually copies the GUI inputs to your command line! So you can just copy and paste the code to a script if needed.
```powershell
Set-Content -Path test.txt -Value HelloWorld
```

If you need extra help with the parameters or the cmdlet click the "?" button at the top right corner to get some extra help!

## Get-Help -ShowWindow

```powershell
Get-Help Get-Process -ShowWindow
```

If you use PowerShell you must know about the `get-help` command, but some people don't know it has a parameter flag call `-ShowWindow`.
This is pretty handy to get the full help of command, get search bar and even Zoom! (sometimes PS font-size is too small when I'm tired)..

![Get-Help ShowWindow Example](/Img/getHelpShowWindow.png

## Out-GridView

```powershell
PS> Get-Process | Out-GridView
```

Sometimes you are working with a LOT of data, and maybe you want do to a manual quick scan and filtering before writing the next piece of code. Well, `Out-GridView` will help you through just that!

![OutGridView Example](/Img/getHelpShowWindow.png)

## Invoke-Item

```powershell
# You can also do "ii ." for short
PS> Invoke-Item .
```

You just got an explorer window on your current path!

## What's Next?

Now you have a new few tools to play in Powershell! I encourage you to keep exploring PowerShell, `get-help` with some `*` here and there is a great way to find out new things. Hope you enjoy this post, See you next time!
