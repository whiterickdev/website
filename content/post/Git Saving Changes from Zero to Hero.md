---
title: "Git Saving Changes From Zero to Hero"
date: 2019-12-01T15:07:29-06:00
lastmod: 2019-12-01T15:07:29-06:00
tags : ["Powershell"]
categories : [ "dev" ]
layout: post
type:  "post"
highlight: false
draft: true
---

## Outline

1. Core Concepts
2. git add
3. git commit
4. Fixing mistakes!