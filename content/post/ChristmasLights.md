---
title: "Bring Christmas to your portfolio with SVG"
date: 2019-12-13T17:41:05-06:00
lastmod: 2019-12-13T17:41:05-06:00
tags : ["svg"]
categories : [ "dev" ]
layout: post
type:  "post"
highlight: false
draft: true
---


<!-- Two weeks before christmas...your decoration at home is ready! All the streets are covered with christmas decoration, and we are runnning to get the gifts for our loved ones, we are almost ready for christmas! Are we?

I'm one of those who got their christmas boxes out on Nov-1st, just after hallowen I have already had my apartment decorated with the christmas lights...and some weeks aho it hit me, I missed to decorate my portfolio page! -->

I love that this time of the year, everything get's some special theme around christmas. Videogames, stores, restaurants...everyone has something special for christmas, why my portfolio page should be different?

I wanted to get some decoration up in my page, and it was the perfect excuse to try out some SVG animations for the first time! here I will take you trhough the process I followed

## Build your decoration

First things first! I needed to build the image that I will loading into my page, I want it to do something simple as this is my first time using SVG in HTML. I used [Inkscape](https://inkscape.org/) and got something like this:

This is the SVG code after saving it as `Plain SVG`

### Animate it!

At quisque litora ullamcorper maecenas ultricies ut dignissim luctus curabitur, cras congue eget primis aliquam fringilla nulla dictum posuere, vestibulum sit magnis nisl praesent eros ipsum nunc. Ligula lacus ipsum orci aenean integer pharetra habitasse interdum, porttitor etiam hac feugiat placerat morbi posuere turpis leo, quam at amet gravida commodo fringilla erat.

## Final thoughts

Look at [my page](erickguillen.dev) for the live decoration! Hurry, it's for limited time! :gift:

You can get the code from Github [here](https://github.com/erickguillen/ChristmasLights)